module Main exposing (..)

import Browser
import Html exposing (Html, div, h1, img, pre, text)
import Html.Attributes exposing (src)



---- MODEL ----


type alias Model =
    {}


init : ( Model, Cmd Msg )
init =
    ( {}, Cmd.none )



---- UPDATE ----


type Msg
    = NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    ( model, Cmd.none )



---- VIEW ----


fond =
    "#.......\n........\n...#....\n........\n........\n........\n........\n........"


type alias Point =
    { x : Int
    , y : Int
    }


type Image
    = Image (List (List Char))

imageString : Image -> String
imageString image =



serpent0 : List Point
serpent0 =
    [ { x = 0, y = 0 } ]


view : Model -> Html Msg
view model =
    div []
        [ pre [] [ text fond ]
        ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }
