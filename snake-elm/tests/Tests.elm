module Tests exposing (..)

import Expect
import Main exposing (..)
import Test exposing (..)



-- Check out https://package.elm-lang.org/packages/elm-explorations/test/latest to learn more about testing in Elm!


image0 =
    Image [ [ '#', '#' ], [ '.', '.' ] ]


all : Test
all =
    describe "ManipulationImage"
        [ test "ConvertionString" <|
            \_ ->
                Expect.equal "##\n..\n" (imageString image0)
        ]
